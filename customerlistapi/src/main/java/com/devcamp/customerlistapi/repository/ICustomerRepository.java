package com.devcamp.customerlistapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerlistapi.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
    
}
