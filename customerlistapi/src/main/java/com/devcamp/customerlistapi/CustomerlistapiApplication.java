package com.devcamp.customerlistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerlistapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerlistapiApplication.class, args);
	}

}
